# Metrodigi Challenge
Movie Database Project

## Requirements

NodeJS 4.3.x or greater.
Ruby 2.0.x or greater.
Ruby Sass Gem 3.4.x or greater.

## Install

Install ruby sass gem.
```javascript
gem install sass
```

Clone this repo and Install packages with:
```javascript
npm install
```

There are two development environments: **develop** and **production**, to launch the dev environment:

```javascript
gulp watch
```

For production environment:

```javascript
gulp watch --production
```

Production Environment does additional steps like minification and uglyfication of code.

Then, navigate to:
```javascript
http://localhost:8080/
```

To compile the project without watching or deploying the web server:
```javascript
gulp build
```
or
```javascript
gulp build --production
```


## Gulp Configuration

Almost all configuration is in build-config.json file at the root of the project, it consists mainly of output folder paths.

Both environments reuses the same gulp pipes. All pipes have conditional statements (gulp-if) to differentiate when they have to output the optimized version (they wait for the --production flag thanks to yargs plugin).

Index.html file uses *gulp-inject* to automatically inject javascript and css into it. Javascript files are parsed by browserify to resolve dependencies.
