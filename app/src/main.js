(function(){
    "use strict";
    var app = angular.module("MetrodigiApp", ["ui.router", "ui.bootstrap", "ngAnimate", "ngMessages"]);
    
    
    
    
    require("./routes")(app);
    
    require("./controllers")(app);
    require("./services")(app);
    require("./directives")(app);
    
})();