module.exports = function(app) {
    var actorSvc = require("./services/database/actors.service"),
    	movieSvc = require("./services/database/movies.service");

    app.service("MoviesService", ["$q", movieSvc]);
    app.service("ActorsService", ["$q", actorSvc]);
};