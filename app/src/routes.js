module.exports = function(app) {
    app.config(["$stateProvider", "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/");
            $stateProvider.state("home", {
                url: "/",
                templateUrl: "src/pages/home/home.html",
                controller: "HomeController",
                resolve: {
                    Movies: ["MoviesService", function(MoviesService) {
                        return MoviesService.getAllMovies().then(function(status) {
                            return status.result;
                        });
                    }]
                    
                }
            })
            .state("movie", {
                url: "/movie/:id",
                templateUrl: "src/pages/movie/movie.html",
                controller: "MovieController",
                resolve: {
                    Movie: ["$stateParams", "MoviesService", function($stateParams, MoviesService) {
                        if ($stateParams.id) {
                            return MoviesService.getMovie($stateParams.id).then(function(status) {
                                return status.result;
                            });
                        } else {
                            return null;
                        }
                    }],
                    Actors: ["$stateParams", "ActorsService", function($stateParams, ActorsService) {
                        return ActorsService.getActorsByMovie($stateParams.id).then(function(status) {
                            return status.result;
                        });
                    }],
                    AllActors: ["ActorsService", function(ActorsService) {
                        return ActorsService.getAllActors().then(function(status) {
                            return status.result;
                        });
                    }]
                }
            })
            .state("actor", {
                url: "/actor/:id",
                templateUrl: "src/pages/actor/actor.html",
                controller: "ActorController",
                resolve: {
                    Actor: ["$stateParams", "ActorsService", function($stateParams, ActorsService) {
                        if ($stateParams.id) {
                            return ActorsService.getActor($stateParams.id).then(function(status) {
                                return status.result;
                            });
                        } else {
                            return null;
                        }
                    }],
                    Movies: ["$stateParams", "MoviesService", function($stateParams, MoviesService) {
                        return MoviesService.getMoviesByActor($stateParams.id).then(function(status) {
                            return status.result;
                        });
                    }],
                    AllMovies: ["MoviesService", function(MoviesService) {
                        return MoviesService.getAllMovies().then(function(status) {
                            return status.result;
                        });
                    }]
                }
            })
            .state("actors", {
                url: "/actorlist",
                templateUrl: "src/pages/actors-list/actors-list.html",
                controller: "ActorsListController",
                resolve: {
                    Actors: ["ActorsService", function(ActorsService) {
                        return ActorsService.getAllActors().then(function(status) {
                            return status.result;
                        });
                    }]
                }
            });


        }
    ]);

};
