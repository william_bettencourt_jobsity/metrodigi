var _ = require("lodash");

module.exports = function($scope, $uibModalInstance, Actors, AddedActors) {
    $scope.Actors = Actors;
    $scope.AddedActors = AddedActors;

    $scope.init = function() {
      _.forEach($scope.AddedActors, function(x) { _.find($scope.Actors, function(y){ return y.Id == x.Id }).selected = true });
    }
    $scope.init();

    $scope.select = function() {
      var selected_actors = _.filter($scope.Actors, function(x){ return x.selected }).map(function(y) { delete y.selected; return y; });
      $uibModalInstance.close(selected_actors);
    }

    $scope.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    }
};