module.exports = function($scope, $uibModal, $state, MoviesService, Movie, Actors, AllActors) {
    $scope.movie = Movie || { Genres: [] };
    $scope.movie.Genres = $scope.movie.Genres.toString();
    $scope.editorEnabled = Movie ? false: true;
    $scope.relatedActors = Actors;
    $scope.allActors = AllActors;
    $scope.amount = 100;


    $scope.enableEditor = function() {
        $scope.editorEnabled = !$scope.editorEnabled;
    }

    $scope.saveMovie = function(movie, actors) {
        movie.Genres = movie.Genres ? movie.Genres.replace(/,\s*,/, ",").replace(/,\s*$/, "").split(',') : [];
        MoviesService.saveOrUpdateMovie(movie, actors).then(function(status){
            $scope.movie.Genres = $scope.movie.Genres.toString();
            $scope.editorEnabled = false;
        });

    }
    $scope.rateThis = function(movie, value) {
        value = value < 0 ? 0 : value;
        value = value > 100 ? 100 : value;
        $scope.amount = value;
        MoviesService.rateMovie(movie.Id, value).then(function(status) {
            if (status.code === 200) {
                $scope.movie = status.result;
                $scope.movie.Genres = $scope.movie.Genres.toString();
            }
        });
    }

    $scope.addNewMovie = function(){
        $state.go("movie");
    }


    $scope.addActors = function() {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: "src/pages/movie/actors-modal.template.html",
            controller: "ActorModalController",
            resolve: {
                Actors: function() {
                    return $scope.allActors; 
                },
                AddedActors: function() {
                    return $scope.relatedActors; 
                }
            }
        });

        modalInstance.result.then(function(selected_actors) {
            $scope.relatedActors = selected_actors;
        });

    }

    $scope.resetForm = function() {

    }

    $scope.deleteMovie = function(movie) {
        MoviesService.deleteMovie(movie.Id).then(function(status){
            if (status.code === 200) {
                $state.go("home");
            }
        });
    }
};
