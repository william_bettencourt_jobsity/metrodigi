var _ = require("lodash");

module.exports = function($scope, $uibModalInstance, Movies, AddedMovies) {
    $scope.Movies = Movies;
    $scope.AddedMovies = AddedMovies;

    $scope.init = function() {
      _.forEach($scope.AddedMovies, function(x) { _.find($scope.Movies, function(y){ return y.Id == x.Id }).selected = true });
    }
    $scope.init();

    $scope.select = function() {
      var selected_movies = _.filter($scope.Movies, function(x){ return x.selected }).map(function(y) { delete y.selected; return y; });
      $uibModalInstance.close(selected_movies);
    }

    $scope.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    }
};