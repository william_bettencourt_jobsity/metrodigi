module.exports = function($scope, $uibModal, $state, ActorsService, Actor, Movies, AllMovies) {
    $scope.actor = Actor || {};
    $scope.editorEnabled = Actor ? false: true;
    $scope.relatedMovies = Movies;
    $scope.allMovies = AllMovies;

    $scope.enableEditor = function() {
        $scope.editorEnabled = !$scope.editorEnabled;
    }

    $scope.saveActor = function(actor, movies) {
        ActorsService.saveOrUpdateActor(actor, movies).then(function(status){
            $scope.editorEnabled = false;
        });

    }

    $scope.addMovies = function() {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: "src/pages/actor/movies-modal.template.html",
            controller: "MovieModalController",
            resolve: {
                Movies: function() {
                    return $scope.allMovies; 
                },
                AddedMovies: function() {
                    return $scope.relatedMovies; 
                }
            }
        });

        modalInstance.result.then(function(selected_movies) {
            $scope.relatedMovies = selected_movies;
        });

    }

    $scope.resetForm = function() {

    }

    $scope.deleteActor = function(actor) {
        ActorsService.deleteActor(actor.Id).then(function(status){
            if (status.code === 200) {
                $state.go("home");
            }
        });
    }
};
