module.exports = function () {
    return {
        restrict: "E",
        scope: {
            item: "="
        },
        templateUrl: 'src/components/directives/actor/actor.directive.html',
        controller: ["$scope", "$state", function($scope, $state) {
            $scope.go = function(where) {
                $state.go("actor", { "id": where });
            }
        }]
    };
};