module.exports = function () {
    return {
    	restrict: "E",
        scope: {
        	item: "="
        },
        templateUrl: 'src/components/directives/movie/movie.directive.html',
        controller: ["$scope", "$state", function($scope, $state) {
        	$scope.go = function(where) {
        		$state.go("movie", { "id": where });
        	}
        }]
    };
};