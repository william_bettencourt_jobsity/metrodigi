module.exports = function(app) {
    var movie = require("./components/directives/movie/movie.directive"),
        actor = require("./components/directives/actor/actor.directive"),
        filereader = require("./components/directives/filereader/filereader.directive");
        reallyclick = require("./components/directives/reallyclick/reallyclick.directive");
    
    app.directive("actorThumbnail", [actor]);
    app.directive("movieThumbnail", [movie]);
    app.directive("fileReader", ["$q", filereader]);
    app.directive("reallyClick", ["$q", reallyclick]);
};