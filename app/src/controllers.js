module.exports = function(app) {
    var HomeCtrl = require("./pages/home/home.controller"),
        MovieCtrl = require("./pages/movie/movie.controller"),
        ActorCtrl = require("./pages/actor/actor.controller"),
        ActorModalController = require("./pages/movie/actors-modal.controller"),
        MovieModalController = require("./pages/actor/movies-modal.controller"),
        ActorsListCtrl = require("./pages/actors-list/actors-list.controller");

    app.controller("HomeController", ["$scope", "Movies", HomeCtrl]);
    app.controller("ActorController", ["$scope","$uibModal", "$state", "ActorsService", "Actor", "Movies", "AllMovies", ActorCtrl]);
    app.controller("MovieController", ["$scope","$uibModal", "$state", "MoviesService", "Movie", "Actors", "AllActors", MovieCtrl]);
    app.controller("ActorModalController", ["$scope","$uibModalInstance", "Actors", "AddedActors", ActorModalController]);
    app.controller("MovieModalController", ["$scope","$uibModalInstance", "Movies", "AddedMovies", MovieModalController]);
    app.controller("ActorsListController", ["$scope", "Actors", ActorsListCtrl]);
};